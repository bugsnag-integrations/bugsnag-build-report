# Bitbucket Pipelines Pipe: Bugsnag build report
 Report information when you build your app to see which release each error was introduced in, and to link from stack traces to the code in your repo.

## YAML Definition
Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
- pipe: bugsnag-integrations/bugsnag-build-report:0.2.2
  variables:
    API_KEY: '<string>'
    # RELEASE_STAGE: '<string>'
    # AUTO_ASSIGN: '<boolean>'
    # APP_VERSION: '<string>'
    # REPO_URL: '<string>'
    # URL: '<string>'
    # DEBUG: '<boolean>'
```

## Variables
| Variable              | Usage                                                       |
| --------------------- | ----------------------------------------------------------- |
| API_KEY (*)   | The API key associated with the project.  |
| RELEASE_STAGE | The release stage (default: 'production'). |
| AUTO_ASSIGN   | 	Whether new errors in this release stage should be automatically assigned to this build. This should only be set to true if you are unable to configure the app version in your notifier (default: 'false'). |
| APP_VERSION   | The version of the app being built (default: $BITBUCKET_BUILD_NUMBER). |
| REPO_URL   | The URL of the Bitbucket repository - if you are using Bitbucket Server, set this to the URL to your Bitbucket repository (optional, default: https://bitbucket.org/${BITBUCKET_REPO_OWNER}/${BITBUCKET_REPO_SLUG}). |
| URL   | The URL to send requests to - If you are using Bugsnag on premise then set this to the build endpoint of your server (default: https://build.bugsnag.com/). |
| DEBUG   | Turn on extra debug information (default: `false`). |

_(*) = required variable._

## Details
By sending the source revision when you build a new version of your app using Bitbucket Pipelines, you'll be able to see which build each error was introduced in and link back to the code in the repo.

## Prerequisites
To use this pipe you must first create a [Bugsnag](http://www.bugsnag.com) account and get your API key.

## Examples
To Trigger build information to be sent when a commit is made on the master branch, update your `bitbucket-pipelines.yml` to include:

```yaml
pipelines:
  branches:
    master:
      - step:
          script:
            - pipe: bugsnag-integrations/bugsnag-build-report:0.2.2
              variables:
                API_KEY: 'YOUR_API_KEY_HERE'
```

## Support
Please contact [support@bugsnag.com](mailto:support@bugsnag.com) with any questions.
