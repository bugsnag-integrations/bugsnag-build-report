#!/usr/bin/env bats

setup() {
  DOCKER_IMAGE=${DOCKER_IMAGE:="test/bugsnag-build-report"}

  echo "Building image..."
  docker build -t ${DOCKER_IMAGE} .
}

teardown() {
    echo "Teardown happens after each test."
}

@test "Test message sent" {
    run docker run \
        -e API_KEY="1900cbface57212307fb0da35542a636" \
        -e BITBUCKET_BUILD_NUMBER="12345" \
        -e BITBUCKET_REPO_OWNER="integrations-test" \
        -e BITBUCKET_REPO_SLUG="test-repository" \
        -e BITBUCKET_COMMIT="12345" \
        -v $(pwd):$(pwd) \
        -w $(pwd) \
        ${DOCKER_IMAGE}

    echo "Status: $status"
    echo "Output: $output"

    [ "$status" -eq 0 ]
}

@test "Test message sent with different repo" {
    run docker run \
        -e API_KEY="1900cbface57212307fb0da35542a636" \
        -e REPO_URL="https://mybitbucket.org/integrations-test/test-repository" \
        -e BITBUCKET_BUILD_NUMBER="12345" \
        -e BITBUCKET_COMMIT="12345" \
        -v $(pwd):$(pwd) \
        -w $(pwd) \
        ${DOCKER_IMAGE}

    echo "Status: $status"
    echo "Output: $output"

    [ "$status" -eq 0 ]
}

@test "Test extra parameters" {
    run docker run \
        -e API_KEY="1900cbface57212307fb0da35542a636" \
        -e RELEASE_STAGE="staging" \
        -e AUTO_ASSIGN="true" \
        -e APP_VERSION="1.2.3" \
        -e BITBUCKET_BUILD_NUMBER="12345" \
        -e BITBUCKET_REPO_OWNER="integrations-test" \
        -e BITBUCKET_REPO_SLUG="test-repository" \
        -e BITBUCKET_COMMIT="12345" \
        -v $(pwd):$(pwd) \
        -w $(pwd) \
        ${DOCKER_IMAGE}

    echo "Status: $status"
    echo "Output: $output"

    [ "$status" -eq 0 ]
}

@test "Test fails with no API key" {
    run docker run \
        -e BITBUCKET_BUILD_NUMBER="12345" \
        -e BITBUCKET_REPO_OWNER="integrations-test" \
        -e BITBUCKET_REPO_SLUG="test-repository" \
        -e BITBUCKET_COMMIT="12345" \
        -v $(pwd):$(pwd) \
        -w $(pwd) \
        ${DOCKER_IMAGE}

    echo "Status: $status"
    echo "Output: $output"

    [ "$status" -eq 1 ]
}
