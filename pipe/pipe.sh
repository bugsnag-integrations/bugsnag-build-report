#!/usr/bin/env bash
#
# This pipe will send build information to Bugsnag.
#
# Required globals:
#   API_KEY      The API key associated with the project
#
# Optional globals:
#   RELEASE_STAGE  The release stage (default: "production”)
#   AUTO_ASSIGN    Whether new errors in this release stage should be automatically assigned to this build. This should only be set to true if you are unable to configure the app version in your notifier (default: "false")
#   APP_VERSION    The version of the app being built (default: $BITBUCKET_BUILD_NUMBER)
#   REPO_URL       The URL of the Bitbucket repository - if you are using Bitbucket Server, set this to the URL to your Bitbucket repository (for bitbucket server default: https://bitbucket.org/${BITBUCKET_REPO_OWNER}/${BITBUCKET_REPO_SLUG})
#   URL            The URL to send requests to - If you are using Bugsnag on premise then set this to the build endpoint of your server (default: https://build.bugsnag.com/)
#   DEBUG          Turn on extra debug information (default: `false`)

source "$(dirname "$0")/common.sh"

debug "Starting to run pipe"

if [ -z "$API_KEY" ]; then
  error "API key must be set"
  exit 1
fi

BUILD_TOOL="bitbucket-pipes"
PROVIDER="bitbucket"
if [ -z "$REPO_URL" ]; then
  BB_REPO_URL="https://bitbucket.org/${BITBUCKET_REPO_OWNER}/${BITBUCKET_REPO_SLUG}"
else
  BB_REPO_URL=${REPO_URL}

  # If this looks like a bitbucket server URL then change the build tool parameter
  if [[ ! "$REPO_URL" =~ ^https://bitbucket\.org.* ]]; then
    BUILD_TOOL="bitbucket-server-pipes"
    PROVIDER="bitbucket-server"
  fi
fi

# default parameters
APP_VERSION=${APP_VERSION:=$BITBUCKET_BUILD_NUMBER}
RELEASE_STAGE=${RELEASE_STAGE:="production"}
AUTO_ASSIGN_RELEASE=${AUTO_ASSIGN:="false"}
URL=${URL:="https://build.bugsnag.com/"}
DEBUG=${DEBUG:="false"}

PAYLOAD="{\"apiKey\":\"${API_KEY}\",\"appVersion\":\"${APP_VERSION}\",\"releaseStage\":\"${RELEASE_STAGE}\",\"autoAssignRelease\":${AUTO_ASSIGN_RELEASE},\"buildTool\":\"${BUILD_TOOL}\",\"sourceControl\":{\"provider\":\"${PROVIDER}\",\"repository\":\"${BB_REPO_URL}\",\"revision\":\"${BITBUCKET_COMMIT}\"}}"

debug "Sending payload: ${PAYLOAD}"

if [[ "${DEBUG}" == "true" ]]; then
  CURL_PARAM="--verbose"
else
  CURL_PARAM="--silent"
fi

OUTPUT=$(curl "${URL}" \
  -X POST \
  -H "Content-Type: application/json" \
  -d "${PAYLOAD}" \
  ${CURL_PARAM})

if [[ "${OUTPUT}" == "{\"status\":\"ok\"}" ]]; then
  success "Sucessfully sent release information to Bugsnag"
  exit 0
else
  fail "Failed to send release information to Bugsnag: ${OUTPUT}"
  exit 1
fi
