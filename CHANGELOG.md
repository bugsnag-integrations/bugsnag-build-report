# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 0.2.2

- patch: Standardise on quotes for pipe variables.

## 0.2.1

- minor: Document DEBUG parameter

## 0.2.0

- minor: Rename to bugsnag-build-report

## 0.1.1

- minor: Updates after review.

## 0.1.0

- minor: Minor README improvements.

## 0.0.15

- minor: Revert pipe.yml to match demo pipes

## 0.0.14

- minor: Add some more fields back into the pipe.yml

## 0.0.13

- minor: Add logo to repository

## 0.0.12

- minor: Try using image

## 0.0.11

- minor: Changed pipe.yml to match other pipes

## 0.0.10

- minor: Added extra URL parameters

## 0.0.9

- minor: Updated tests

## 0.0.8

- minor: Updated readme

## 0.0.7

- minor: Fixes

## 0.0.6

- minor: Add test output for json

## 0.0.5

- minor: Sync version numbers

## 0.0.4

- minor: Change repo

## 0.0.3

- minor: Add docker release back in

## 0.0.2

- minor: Next version

## 0.0.1

- minor: Initial version

